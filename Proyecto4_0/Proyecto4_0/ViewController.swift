//
//  ViewController.swift
//  Proyecto4_0
//
//  Created by dedam on 30/1/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var toolbar: UIView!
    
    @IBOutlet weak var btn_mas: UIButton!
    @IBOutlet weak var btn_send: UIButton!
    
    let imgPick = UIImagePickerController()
    var mylabel:[[ChatData]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableview.dataSource = self
        self.tableview.delegate = self
        self.imgPick.delegate = self
        var section1:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataMessage
            newChat.id = i
            
            newChat.description = "A les 8:00 hem quedat al restaurant per sopar, tinc taula reservada, hem fa molta il·lusió veuret avui "
            
            
            section1.append(newChat)
        }
        
        var section2:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataMessage1
            newChat.id = i
            
            newChat.description = "D'acord, a mi també :)"
            
            
            section2.append(newChat)
        }
        
        var section3:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataImage
            newChat.id = i
            
            newChat.imgName = "company"
            
            
            section3.append(newChat)
        }
        var section4:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataImage1
            newChat.id = i
            
            newChat.imgName = "company"
            
            
            section4.append(newChat)
        }
        mylabel.append(section1)
        mylabel.append(section2)
        mylabel.append(section3)
        mylabel.append(section4)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_send_pressed(_ sender: Any) {
        var section5:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataMessage
            newChat.id = i
            
            newChat.description = "mensaje test"
            
            
            section5.append(newChat)
        }
        var section6:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataMessage1
            newChat.id = i
            
            newChat.description = "mensaje test"
            
            
            section6.append(newChat)
        }
        mylabel.append(section5)
        mylabel.append(section6)
        tableview.reloadData()
        
    }
    @IBAction func btn_mas_pressed(_ sender: Any) {
        imgPick.allowsEditing = false
        imgPick.sourceType = .photoLibrary
        //imgPick.sourceType = .camera
        present(imgPick,animated: true, completion: nil)
        
        present(imgPick,animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        var section7:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataImage
            newChat.id = i
            newChat.picGal = img
           
            section7.append(newChat)
    }
        var section8:[ChatData] = []
        for i in 0..<1{
            let newChat:ChatData = ChatData.init()
            newChat.normalType = ChatData.m_chatDataType.chatDataImage1
            newChat.id = i
            newChat.picGal = img
            
            section8.append(newChat)
        }
        mylabel.append(section7)
        mylabel.append(section8)
        dismiss(animated: true, completion: nil)
        tableview.reloadData()
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return mylabel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mylabel[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat = mylabel[indexPath.section][indexPath.row]

       
        if chat.normalType == ChatData.m_chatDataType.chatDataMessage{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyMessage_Cell", for: indexPath)
            let myCell = cell as! Chat_MyMessage_Cell
            
            myCell.id = chat.id
            myCell.label_mymessage.text = chat.description
            myCell.vc = self
            
            return cell
        } else if chat.normalType == ChatData.m_chatDataType.chatDataMessage1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_OtherMessage_Cell", for:indexPath)
            let myCell = cell as! Chat_OtherMessage_Cell
            
            myCell.lbl_other.text = chat.description
            
            
            return cell
            
        
        }else if chat.normalType == ChatData.m_chatDataType.chatDataImage{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyImage_Cell", for:indexPath)
            
            let myCell = cell as! Chat_MyImage_Cell
            
            
            myCell.company_mi.image = UIImage(named:chat.imgName)
            return cell
            
        
        
        }else if chat.normalType == ChatData.m_chatDataType.chatDataImage1 {
    
            let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_OtherImage_Cell", for:indexPath)
            let myCell = cell as! Chat_OtherImage_Cell
    
            myCell.imgCompany2.image = UIImage(named:chat.imgName)
            return cell
    }
        else{
            chat.normalType = ChatData.m_chatDataType.chatDataNone
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Chat_MyMessage_Cell", for: indexPath)
            let myCell = cell as! Chat_MyMessage_Cell
            
            myCell.id = chat.id
            myCell.label_mymessage.text = ""
            myCell.vc = self
            
            return cell
            
        }
    
    }
    
}

