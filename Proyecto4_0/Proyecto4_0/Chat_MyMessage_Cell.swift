//
//  Chat_MyMessage_Cell.swift
//  Proyecto4_0
//
//  Created by dedam on 31/1/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit

class Chat_MyMessage_Cell: UITableViewCell {
    var id:Int = -1
    var vc:ViewController? = nil
    
    @IBOutlet weak var globo_a: UIImageView!
    
    @IBOutlet weak var globo_b: UIImageView!
    
    @IBOutlet weak var globo_c: UIImageView!
    @IBOutlet weak var globo_d: UIImageView!
    
    @IBOutlet weak var label_mymessage: UILabel!
    
    @IBOutlet weak var hora_mm: UIVisualEffectView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
