//
//  ChatData.swift
//  Proyecto4_0
//
//  Created by dedam on 3/2/18.
//  Copyright © 2018 dedam. All rights reserved.
//

import UIKit


class ChatData{
    
    enum m_chatDataType {
        case chatDataMessage
        case chatDataMessage1
        case chatDataImage
        case chatDataImage1
        case chatDataNone
    }
    var m_bIsMine:Bool
    var normalType = m_chatDataType.chatDataMessage
    var description:String
    var id:Int
    var imgName:String
    var picGal: UIImage?
    
    init(){
        id = -1
        normalType = m_chatDataType.chatDataMessage
        description = ""
        m_bIsMine = true
        imgName = "company"
        picGal = nil
        
    }
    
    
    
}
